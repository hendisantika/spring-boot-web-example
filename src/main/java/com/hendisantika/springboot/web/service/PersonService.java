package com.hendisantika.springboot.web.service;

import com.hendisantika.springboot.web.model.Person;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class PersonService {

	private static Map<Integer, Person> personDB;

	@PostConstruct
	public void init() throws Exception {
		personDB = new HashMap<>();
		for (int i = 0; i < 100; i++) {
			Person person = new Person(i, "Person-name-" + i, System.currentTimeMillis());
			personDB.put(new Integer(i), person);
		}
	}

	public List<Person> getAllPersons() {
		// Java 8, Convert all Map values  to a List
		List<Person> personList = personDB.values().stream()
				.collect(Collectors.toList());
		return personList;
	}

	public Person getPersonById(Integer id) {
		return personDB.get(id);
	}
}
