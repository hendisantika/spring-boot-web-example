package com.hendisantika.springboot.web.controller;

import com.hendisantika.springboot.web.model.Person;
import com.hendisantika.springboot.web.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/search")
public class PersonAPI {

	@Autowired
	private PersonService personService;

	@GetMapping("/persons")
	public List<Person> showAllStudents() {
		return personService.getAllPersons();
	}

	@GetMapping("/person")
	public Person searchStudent(@RequestParam(name = "personId", required = true)
										Integer personId) {
		Person person = personService.getPersonById(personId);
		return person;
	}

}
