# Spring Boot Web

Open your browser :

List All Persons:

`http://localhost:8080/search/persons`


List Person By ID:

`http://localhost:8080/search/person?personId=1`

```
// 20170427103746
// http://localhost:8080/search/person?personId=1

{
  "personId": 1,
  "name": "Person-name-1",
  "ssn": 1493264177342
}

```